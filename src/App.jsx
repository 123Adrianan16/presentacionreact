import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import ComponenteU from './Components/ComponenteU'
import ComponenteD from './Components/ComponenteD'
import ComponenteT from './Components/ComponenteT'

function App() {

  return (
    <div className="Inicio">
      <ComponenteU/>
      <ComponenteD/>
      <ComponenteT/>
    </div>
  )
}

export default App
