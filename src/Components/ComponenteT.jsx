import React from 'react'
import ImagenF from '../assets/facebook.png'
import ImagenI from '../assets/instagram.png'
import ImagenT from '../assets/tiktok.png'

export default function ComponenteT() {
    return (
        <div class="redes">
            <a href="https://www.facebook.com/monse.najera.545">
                <img src={ImagenF} alt="facebook" height="80" width="80" /></a>

            <a href="https://www.instagram.com/adriana_najerag/?fbclid=IwAR2JXyghbvOJeW1fjbsn_CS60IZTEbdH4WNV0xAVUzWNg1WZuUILhuXdshc">
                <img src={ImagenI} alt="instagram" height="80" width="80" /></a>

            <a href="https://www.tiktok.com/@adriananajera16?_t=8ZpwdtIzaxK&_r=1">
                <img src={ImagenT} alt="tiktok" height="80" width="80" /></a>

        </div>
    )
}
