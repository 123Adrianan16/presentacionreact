import React from 'react'
import Imagen from '../assets/1.jpg'

export default function ComponenteD() {
  return (
    <div  class="Contenedor">
        <img src={Imagen} alt="" height="150" width="150" flex="100" />

        <div class="Info">
            <h2 class="dos">Nombre: Adriana Monserrath Najera Gomez</h2>
            <h2>No.C: 19680209</h2>
            <h2>Carrera: ISC</h2>
            <h2>Edad: 21</h2>
            <h2>Semestre: 8</h2>
        </div>
    </div>
  )
}
